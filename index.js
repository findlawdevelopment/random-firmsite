var request = require('request')
  , _ = require("underscore")
  , Q = require("q")
;

var attempts = 0;

function getSite () {
  var deferred = Q.defer();

  var makeRequest = function () {
    var random = Math.ceil(Math.random() * 200);
    
    request.get('http://livesearch.int.thomson.com/api/search/' + random, function (err, res, body) {
      if (!err && res.statusCode === 200) {

        var data = findFirmsites(body);
        
        if (data.length > 0) {

          deferred.resolve(_.sample(data));
          
        } else {

          attempts++;
          if (attempts < 6) {
            deferred.notify('No data returned. Trying again...');
            makeRequest();
          } else {
            deferred.reject('Request failed 5 times. I Give up!');
          }

        }


      } else {

        attempts++;
        if (attempts < 6) {

          deferred.notify('Request failed. Trying again...');
          makeRequest();

        } else {

          deferred.reject('Request failed 5 times. I give up!');

        }
      }
    });
  };

  makeRequest();

  return deferred.promise;
}

function findFirmsites (subs) {
  subs = JSON.parse(subs);

  var newSubs = [];

  subs.forEach(function (sub) {
    if (sub.domain && sub.description.match(/firmsite/i)) {
      newSubs.push(sub);
    }
  })

  return newSubs;
}

module.exports = getSite;